angular.module('albumsApp').service 'ObjectService',
  class ObjectService 
    constructor: ->
      @items = []
      @item = {}
      
    setItems: (items) ->
      @items = items
    
    setItem: (item) ->
      @item = item
      
    addItem: (item)->
      @items.push item
      
    removeItem: (item) ->
      index = @items.indexOf item
      @items.splice index, 1
      
    isEmpty: ->
      return @items.length == 0
      
    isNotEmpty: ->
      return !@isEmpty()


