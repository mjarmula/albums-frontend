angular
.module('albumsApp')
.factory "Resource", Array 'RailsResource', '$q', '$cookies', (RailsResource, $q, $cookies) ->
  class Resource extends RailsResource
    @excluded_attrs: ["user_id", 'base']
    @baseUrl = "http://localhost:8000/"
    @configure
      # httpConfig:
      #   headers:
      #     'X-CSRF-TOKEN': $cookies.get('XSRF-TOKEN')
      # ,  
      interceptors: [{
        afterResponseError: (rejection, resourceConstructor, context) =>
          if angular.isObject(rejection.data)
            for attr, obj of rejection.data
              for message in obj
                message = if attr not in @excluded_attrs then attr + ' ' + message else message
          return $q.reject(rejection)
        ,
        afterResponse: (result, resourceConstructor, context)->
          return result;
      }]