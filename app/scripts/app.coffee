'use strict'

###*
 # @ngdoc overview
 # @name albumsApp
 # @description
 # # albumsApp
 #
 # Main module of the application.
###
angular
  .module('albumsApp', [
    'ngAnimate',
    'ngCookies',
    'ngSanitize',
    'ui.router',
    'rails',
    'mgcrea.ngStrap',
    'ngFlash'
  ])
  .config(($stateProvider, $urlRouterProvider)->
      $urlRouterProvider.otherwise '/home'
      $stateProvider
        .state('home',
          url: '/home',
          templateUrl: 'views/home/index.html'
        )
        .state('albums',
          url: '/albums',
          templateUrl: 'views/albums/index.html',
          controller: 'AlbumsCtrl',
          controllerAs: 'ac'
        )
        .state('showAlbum',
          url: '/albums/show/:id',
          templateUrl: 'views/albums/show.html',
          controller: 'AlbumCtrl',
          controllerAs: 'asc',
          highlight: 'albums'
        )
  )
