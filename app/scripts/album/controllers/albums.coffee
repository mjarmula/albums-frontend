class AlbumsCtrl
  @$inject: ['AlbumResource', 'AlbumService', '$scope', 'Flash']
  constructor: (@AlbumResource, @AlbumService, @scope, @Flash)->
    @AlbumService.item = new @AlbumResource()
    @album = @AlbumService
    @AlbumResource.query().then (data)=>
      @AlbumService.setItems(data)
      @album = @AlbumService
      
  save: (callback)->
    @album.item.create().then (data) =>
      @album.item = new @AlbumResource()
      @album.addItem(data)
      @Flash.create('success', 'Album has been created successfully')
      callback()
    @albumForm.$setPristine()
  
  destroy: (album)->
    album.delete().then () =>
      @album.removeItem(album)
      @Flash.create('warning', 'Album has been destroyed successfully')

angular
  .module('albumsApp')
  .controller 'AlbumsCtrl', AlbumsCtrl