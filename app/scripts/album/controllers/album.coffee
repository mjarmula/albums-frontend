class AlbumCtrl
  @$inject: ['AlbumResource', 'AlbumService', '$state']
  constructor: (@AlbumResource, @AlbumService, @state)->
    @album = @AlbumService
    @AlbumResource.get(@state.params.id).then (data) =>
      @album.setItem(data)
angular.module('albumsApp').controller 'AlbumCtrl', AlbumCtrl 