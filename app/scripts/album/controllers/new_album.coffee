class NewAlbumCtrl
  @$inject: ['$scope', 'AlbumResource', 'Flash']
  
  constructor: (@scope, @AlbumResource, @Flash)->
    @album = new @AlbumResource()
  
  save: (callback)->
    @album.create().then (data)=>
      @scope.$parent.$hide()
      callback()
      @Flash.create('success', 'Album has been created successfully');
    @albumForm.$setPristine()

angular.module('albumsApp').controller 'NewAlbumCtrl', NewAlbumCtrl 
