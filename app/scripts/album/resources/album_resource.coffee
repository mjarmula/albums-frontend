angular
  .module('albumsApp')
  .factory 'AlbumResource', Array 'Resource', (Resource)->
    class AlbumResource extends Resource
      @configure
        url: @baseUrl + 'album',
        name: 'album'