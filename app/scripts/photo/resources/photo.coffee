angular
  .module('albumsApp')
  .factory 'PhotoResource', Array 'Resource', (Resource)->
    class PhotoResource extends Resource
      @configure
        url: @baseUrl + 'photo',
        name: 'photo'