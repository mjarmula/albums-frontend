class PhotosCtrl
  @inject: ['AlbumService', 'PhotoResource', 'Flash']
  constructor: (@AlbumService, @PhotoResource, @Flash)->
    
  removePhoto: (photo)->
    new @PhotoResource(photo).delete().then =>
      index = @AlbumService.item.photos.indexOf photo
      @AlbumService.item.photos.splice(index,1)
      @Flash.create('warning', 'Photo has been successfully removed')
angular.module('albumsApp').controller 'PhotosCtrl', PhotosCtrl