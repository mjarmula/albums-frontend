class PhotoCtrl
  @$inject: ['PhotoResource', 'PhotoService', 'AlbumService', 'Flash']
  constructor: (@PhotoResource, @PhotoService, @AlbumService, @Flash)->
    @photo = @PhotoService
    @photo.item = new @PhotoResource
  
  save:(callback)->
    @photo.item.album_id = @AlbumService.item.id
    photoResource = new @PhotoResource @photo.item
    photoResource.create().then (data)=>
      @AlbumService.item.photos.push data
      @Flash.create('success', 'Photo has been successfully added to the album');
      callback();
angular.module('albumsApp').controller 'PhotoCtrl', PhotoCtrl