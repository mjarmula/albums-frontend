
angular.module('albumsApp').service 'TopMenuResource', ->
  new class TopMenuResource
    menu: 
      home : new MenuElement( 
        title: 'Home',
        state: 'home'
      )
      ,
      albums : new MenuElement(
        title: 'Albums',
        state: 'albums'
      )
            
    makeActive: (state, pos, newVal)->
      if state == newVal
        pos.addClass 'active'
      else
        pos.removeClass 'active'
    


class MenuElement
  constructor: (opts) ->
    angular.extend @, opts
    @class = []

  addClass: (className)->
    @class.push className  if @class.indexOf(className) == -1
      
    
  removeClass: (className)->
    index = @class.indexOf className
    @class.splice index, 1
    
  getClass: ->
    @class.join(' ')
  
  type: ->
    if @nested
      'nested'
    else
      'regular'




