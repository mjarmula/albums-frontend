class TopMenuComponent
  constructor: (@scope, @state, @TopMenuResource)->
    @scope.state = @state
    @scope.menu = @TopMenuResource.menu
    @scope.$watch 'state.current.name', (newVal, oldVal) =>
       if newVal != oldVal
         for state, pos of @TopMenuResource.menu
           stateName = @state.current.highlight || newVal
           @TopMenuResource.makeActive state, pos, stateName

angular
  .module('albumsApp')
  .component 'topMenu', 
    controller: ['$scope', '$state', 'TopMenuResource', TopMenuComponent],
    templateUrl: '/scripts/components/top_menu/views/top_menu.html',
